
function generateTable() {
    fetch('http://dummy.restapiexample.com/api/v1/employees')
        .then(response => {
            if (response.status === 200) {
                return response.json();
            } else {
                alert("No hay datos");
            }
        })
        .then(data => {
            for (var j = 0; j < data["data"].length; j++) {
                const cell1 = document.createElement('td');
                const cell2 = document.createElement('td');
                const tr = document.createElement('tr');
                const tbody = document.querySelector('table>tbody');
                cell1.textContent = data["data"][j]["employee_name"];
                cell2.textContent = data["data"][j]["employee_salary"];
                tr.appendChild(cell1);
                tr.appendChild(cell2);
                tbody.appendChild(tr);
            }
        })
}


class ButtonG extends HTMLButtonElement{
    constructor(){
        super();
        this.addEventListener('click', function(e){
                generateTable();
        })
    }

    static get ce_Name(){
        return 'mi-boton-extendido';
    }

    get is (){
        return this.getAttribute('is');
    }

    set is (value){
        this.setAttribute('is', value|| this.ce_Name);
    }

}

var inputIndex = document.createElement('input')
inputIndex.setAttribute('placeholder',"Introduce tu busqueda");
document.querySelector('#botones').appendChild(inputIndex);

const generateTableButton = document.createElement('button',{is: ButtonG.ce_Name});
generateTableButton.addEventListener('click',(e)=>{
        generateTable();
});
generateTableButton.textContent = "Genera tabla";
document.querySelector('#botones').appendChild(generateTableButton);

const searchIndexButton = document.createElement('button',{is: ButtonG.ce_Name});
searchIndexButton.addEventListener('click',(e)=>{
 
        search();
});
searchIndexButton.textContent = "Busca indice";
document.querySelector('#botones').appendChild(searchIndexButton);

const deleteSrch = document.createElement('button',{is: ButtonG.ce_Name});
deleteSrch.addEventListener('click',(e)=>{
        deleteSearch();
});
deleteSrch.textContent = "Borrar Busqueda";
document.querySelector('#botones').appendChild(deleteSrch);

function deleteSearch(){
    var tbody = document.querySelector("table>tbody")
    // tbody.removeChild(tbody.childNodes[0]);
    if(tbody.hasChildNodes()){
        tbody.innerHTML =" ";
        inputIndex.value = " "
    }
}

function search(){
    var input, filter,table, tr, td, i, txtValue;
    input = inputIndex.value.toUpperCase();
    filter = input;
    table = document.getElementsByTagName("table")[0];
    tr = document.getElementsByTagName("tr");
    
    
    for(i=0;i<tr.length;i++){
        td = tr[i].getElementsByTagName("td")[0];
        if(td){
            txtValue = td.textContent || td.innerText;
            if(txtValue.toUpperCase().indexOf(filter)>-1){
                tr[i].style.display ="";
            }else{
                tr[i].style.display = "none";
            }
        }
    }
}